from django.urls import path

from .api_views import api_list_attendees, api_show_attendee

urlpatterns = [
    # This URL creates an attendee
    path("attendees/", api_list_attendees, name="api_create_attendees"),
    # This URL lists all the attendees or a specific on given an id.
    path(
        "conferences/<int:conference_vo_id>/attendees/",
        api_list_attendees,
        name="api_list_attendees",
    ),
    # This URL shows the details of a specific attendee.
    path("attendees/<int:id>/", api_show_attendee, name="api_show_attendee"),
]

from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }


# Lists the attendees' names.
@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    # if block only runs if the HTTP request.method is GET
    if request.method == "GET":
        attendees = Attendee.objects.filter(
            conference=conference_vo_id
        )  # https://www.w3schools.com/django/django_queryset_filter.php
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeDetailEncoder
        )
    # else block handles the POST request
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            # if statement works if you use the path "attendees/"
            if conference_vo_id is None:
                conference_href = (
                    f"/api/conferences/{content['conference_id']}/"
                )
            # else statement works if you use the path "conferences/<int:conference_vo_id>/attendees/"
            else:
                conference_href = f"/api/conferences/{conference_vo_id}/"

            # The way Learn intended us to do
            # This works if you use the path "conferences/<int:conference_vo_id>/attendees/"
            # conference_href = f'/api/conferences/{conference_vo_id}/'

            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference

        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


# Returns the details for the Attendee model specified by the id parameter.
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # if block only runs if the HTTP request.method is GET
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )

    # elif statement tests if the HTTP method is DELETE. Deletes the object and return a JsonResponse indicating if anything got deleted.
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(
            id=id
        ).delete()  # https://docs.djangoproject.com/en/4.0/ref/models/querysets/#delete
        return JsonResponse({"deleted": count > 0})

    # else block handles the PUT request
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            if "conference" in content:
                conference = ConferenceVO.objects.get(id=content["conference"])
                content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

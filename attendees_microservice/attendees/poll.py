import json
import requests

from .models import ConferenceVO


# • Constantly checking for the database in the monolith.
# • It's a GET request. If it already exists, then it'll update (line 14) the
#   microservice.
# • If it doesn't yet exist, then it'll create (line 14) the microservice with
#   the data from the original (in the monolith).
def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )

from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        # isinstance() Function: https://www.w3schools.com/python/ref_func_isinstance.asp
        if isinstance(o, datetime):
            # return o.isoformat()
            return o.isoformat()
        # otherwise return super().default(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    # Ties back to the encoders property in the ConferenceDetailEncoders class
    # and LocationListEncoder class in events/api_views
    encoders = {}

    def default(self, o):
        # if the object to decode is the same class as what's in the
        # model property, then
        if isinstance(o, self.model):
            # * create an empty dictionary that will hold the property names
            # as keys and the property values as values
            d = {}
            # if o has the attribute get_api_url
            # hasattr() Function: https://www.w3schools.com/python/ref_func_hasattr.asp
            if hasattr(o, "get_api_url"):
                #    then add its return value to the dictionary
                #    with the key "href"
                d["href"] = o.get_api_url()
            # * for each name in the properties list
            for property in self.properties:
                # * get the value of that property from the model instance
                # given just the property name
                # getattr() Function: https://www.w3schools.com/python/ref_func_getattr.asp
                value = getattr(o, property)
                # Ties back to encoders property in api_views
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                # * put it into the dictionary with that property name as
                # the key
                d[property] = value
            # Method to handle edge cases of putting the name of the status in the api_list_presentations
            # and putting the state abbreviation in the response for api_show_location.
            d.update(self.get_extra_data(o))
            # * return the dictionary
            return d
        # otherwise, return super().default(o)  # From the documentation
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}

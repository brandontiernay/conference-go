from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


# Uses the Pexels API
# Takes city/state information that comes in on the new location itself
# api_list_locations view function ends up running our helper class because it allows a POST request.
def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    query = f"{city} {state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    # Make the request
    response = requests.get(url, headers=headers)
    # Parse the JSON response https://requests.readthedocs.io/en/latest/user/quickstart/#json-response-content
    picture_url = response.json()["photos"][0]["src"]["original"]
    # Return a dictionary that contains a 'picture_url' key and
    # one of the URLs for one of the picture in the response.
    return {"picture_url": picture_url}


# Uses the Open Weather API
def get_lat_lon(location):
    """
    Return the latitude and longitude for the specified location
    using the OpenWeatherMap API.
    """
    # construct the prams to include in the url
    params = {
        "q": f"{location.city}, {location.state.abbreviation}, USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    #  define the url itself with the params
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # make the request
    response = requests.get(url, params)
    # parse the response and pull out the lat and lon coordinates
    lat = response.json()[0]["lat"]
    lon = response.json()[0]["lon"]
    return {
        "lat": lat,
        "lon": lon,
    }


# Uses the Open Weather API
def get_weather_data(location):
    """
    Returns the current weather data for the specified location
    using the OpenWeatherMap API.
    """
    # get the lat and lon coordinates for the location
    lat_lon = get_lat_lon(location)
    if lat_lon is None:
        return None
    # construct the params to include the url
    params = {
        "lat": lat_lon["lat"],
        "lon": lat_lon["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # define the url itself with the aprams
    url = "https://api.openweathermap.org/data/2.5/weather"
    # make request
    response = requests.get(url, params)
    # parse the response and pull out the lat and lon coordinates
    description = response.json()["weather"][0]["description"]
    temp = response.json()["main"]["temp"]
    # return the temp and description
    return {
        "description": description,
        "temp": temp,
    }

from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from events.api_views import ConferenceListEncoder, Conference
from django.views.decorators.http import require_http_methods
import json


# Lists the presentation titles.
class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]


# Returns the details for the Presentation model specified by the id parameter.
class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    # if block only runs if the HTTP request.method is GET
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations}, encoder=PresentationListEncoder
        )

    # else block handles the POST request
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


# Returns the details for the Presentation model specified by the id parameter.
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # if block only runs if the HTTP request.method is GET
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

    # elif statement tests if the HTTP method is DELETE. Deletes the object and return a JsonResponse indicating if anything got deleted.
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(
            id=id
        ).delete()  # https://docs.djangoproject.com/en/4.0/ref/models/querysets/#delete
        return JsonResponse({"deleted": count > 0})

    # else block handles the PUT request
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference

        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        # Goes look at Presentation model and filters it by specified of the presentation id and updates content
        Presentation.objects.filter(id=id).update(**content)
        # Save the above into a variable to return it
        presentation = Presentation.objects.get(id=id)

        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
